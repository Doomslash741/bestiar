package main.git.doomshade.bestiar;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import main.git.doomshade.bestiar.cmds.CommandHandler;
import main.git.doomshade.bestiar.data.PlayerData;
import main.git.doomshade.bestiar.listeners.PlayerListeners;
import main.git.doomshade.bestiar.utils.MythicMobUtils;

public class Bestiar extends JavaPlugin implements Listener {
	private static Bestiar instance;
	private File playersFile, mobsFolder, mobsFile;
	public static final long PERIOD = 20 * 120;

	public File getMobsFolder() {
		return mobsFolder;
	}

	@Override
	public void onEnable() {
		setInstance(this);
		// TODO Auto-generated method stub
		saveDefaultConfig();
		init();
		CommandHandler.setup();
		Bukkit.getPluginManager().registerEvents(new PlayerListeners(), this);

	}

	public static PlayerData getPlayerData(OfflinePlayer hrac) {
		return PlayerData.getPlayerData(hrac);
	}

	public void init() {
		initFiles();
		getMythicMobUtils().initData();
	}

	public static MythicMobUtils getMythicMobUtils() {
		return MythicMobUtils.getInstance();
	}

	public File getPlayerFile() {
		return playersFile;
	}

	public File getMobsFile() {
		return mobsFile;
	}

	public List<String> getTowns() {
		return getConfig().getStringList("mesta");
	}

	private void initFiles() {

		this.playersFile = new File(getDataFolder(), "playerdata.yml");
		if (!getDataFolder().isDirectory()) {
			getDataFolder().mkdirs();
		}

		this.mobsFolder = new File(getDataFolder(), "Mobs");
		if (!mobsFolder.isDirectory()) {
			mobsFolder.mkdir();
		}
		if (!playersFile.exists()) {
			try {
				playersFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		mobsFile = new File(getDataFolder(), "mobs.yml");
		if (!mobsFile.exists()) {
			try {
				mobsFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onDisable() {
		Bukkit.getScheduler().cancelTask(PlayerData.getTask());
		// TODO Auto-generated method stub
		try {
			PlayerData.save();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @return the instance
	 */
	public static Bestiar getInstance() {
		return instance;
	}

	/**
	 * @param instance
	 *            the instance to set
	 */
	private static void setInstance(Bestiar instance) {
		Bestiar.instance = instance;
	}
}

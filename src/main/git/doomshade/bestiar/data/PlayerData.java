package main.git.doomshade.bestiar.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.Inventory;

import main.git.doomshade.bestiar.Bestiar;
import main.git.doomshade.bestiar.MythicMob;
import main.git.doomshade.bestiar.gui.guis.MainGUI;
import main.git.doomshade.bestiar.utils.Utils;

public class PlayerData {
	private static Bestiar plugin;
	private static FileConfiguration loader;
	private static final HashMap<UUID, PlayerData> PLAYERS = new HashMap<>();
	private static int task = -1;
	private Map<Integer, Inventory> inventories;
	private List<String> activeNames;
	private final OfflinePlayer hrac;
	private final UUID uuid;

	static {
		plugin = Bestiar.getInstance();
		loader = YamlConfiguration.loadConfiguration(plugin.getPlayerFile());
	}

	private PlayerData(final OfflinePlayer hrac) {
		this.hrac = hrac;
		this.uuid = hrac.getUniqueId();
		if (!loader.isList(uuid.toString())) {
			loader.set(uuid.toString(), new ArrayList<String>());
		}
		load();
	}

	public static int getTask() {
		return task;
	}

	private void load() {
		loadInventories();
		PLAYERS.put(hrac.getUniqueId(), this);
	}

	private void loadInventories() {
		this.inventories = new HashMap<>();
		this.activeNames = new ArrayList<>(loader.getStringList(uuid.toString()));
		loadFirstInv();
	}

	private void loadFirstInv() {
		Inventory inv = Bukkit.createInventory(null, 9 * 6, Utils.FIRST_MENU_NAME);
		for (MythicMob mob : Bestiar.getMythicMobUtils().getMythicMobs()) {
			if (!isOp() && !hasKilled(mob)) {
				inv.addItem(mob.getActiveItem());
			} else {
				inv.addItem(mob.getInactiveItem());
			}
		}
		inventories.put(0, inv);
	}

	public static PlayerData getPlayerData(OfflinePlayer hrac) {
		return PLAYERS.getOrDefault(hrac, new PlayerData(hrac));
	}

	public List<String> getKilledMobNames() {
		return activeNames;
	}

	public boolean hasKilled(MythicMob mob) {
		return hasKilled(mob.getName());
	}

	public boolean hasKilled(String mobName) {
		return mobName.isEmpty() ? false : activeNames.contains(ChatColor.stripColor(mobName)) || isOp();
	}

	public boolean isOp() {
		return hrac.isOp() || hrac.getPlayer().hasPermission("bestiar.*");
	}

	public void addKill(MythicMob mob) {
		String mobName = ChatColor.stripColor(mob.getName());
		UUID uuid = hrac.getUniqueId();
		if (hasKilled(mob)) {
			return;
		}
		if (hrac.isOnline()) {
			hrac.getPlayer().sendMessage(mob.getName() + ChatColor.RESET + " pridan do Bestiare");
		}
		activeNames.add(mobName);
		loader.set(uuid.toString(), activeNames);
	}

	public Map<Integer, Inventory> getInventories() {
		return inventories;
	}
	
	public void openInventory() {
		openInventory(0);
	}
	
	public void openInventory(int index) {
		if (!hrac.isOnline()) {
			return;
		}
		if (!inventories.containsKey(index)) {
			return;
		}
		new MainGUI("", hrac.getPlayer()).openInventory();
	}

	public static void loadData() {
		task = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					loader.save(plugin.getPlayerFile());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}, 0, Bestiar.PERIOD);
	}

	public static void save() throws IOException {
		loader.save(plugin.getPlayerFile());
	}
}

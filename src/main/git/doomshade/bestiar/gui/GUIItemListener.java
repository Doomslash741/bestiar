package main.git.doomshade.bestiar.gui;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

public interface GUIItemListener {

	public void onInteract(Player player, String itemName, ClickType click); 
}

/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.ChatColor
 *  org.bukkit.Material
 *  org.bukkit.entity.Player
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.Listener
 *  org.bukkit.event.inventory.InventoryClickEvent
 *  org.bukkit.event.player.AsyncPlayerChatEvent
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 *  org.bukkit.plugin.Plugin
 */
package main.git.doomshade.bestiar.gui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import main.git.doomshade.bestiar.Bestiar;
import main.git.doomshade.bestiar.events.BestiarClickEvent;
import main.git.doomshade.bestiar.listeners.PlayerListeners;

public abstract class GUIApi implements Listener {
	protected static final ArrayList<UUID> PLAYERS_CHAT = new ArrayList<>();
	protected ItemStack clickedItem = null;
	public static final HashMap<UUID, HashMap<String, GUIApi>> GUIS_BY_NAME = new HashMap<>();
	private String name;
	protected StringBuilder context;
	private LinkedHashMap<Integer, Inventory> inventories;
	private List<GUIClickedItem> setupInventory;
	protected static final char SEPARATOR = ';';
	private Map<UUID, GUIApi> previousGui;
	protected Player hrac;

	public GUIApi(String context, Player hrac) {
		this.context = new StringBuilder(context);
		previousGui = new HashMap<>();
		this.hrac = hrac;
		registerGui(this, hrac.getUniqueId());
		
	}

	protected void setSetupInventory(List<GUIClickedItem> inv) {
		this.setupInventory = inv;
	}

	protected abstract void setupInventory();

	// 1) GUI; 2) itemName; 3) gui after item click
	public static Map<GUIApi, Map<String, GUIApi>> GUIS = new HashMap<>();

	public void setName(String name) {
		setName(name, false);
	}

	public void setName(String name, boolean updateInventory) {
		this.name = name;
		if (updateInventory) {
			update();
		}
	}
	
	public StringBuilder getContext() {
		return context;
	}
	
	public void setPreviousGui(GUIApi gui) {
		previousGui.put(hrac.getUniqueId(), gui);
	}
	
	public GUIApi getPreviousGui() {
		return previousGui.get(hrac.getUniqueId());
	}
	
	private void update() {
		List<ItemStack> contents = new ArrayList<>();

		for (GUIClickedItem item : setupInventory) {
			contents.add(item.getItem());
		}
		this.inventories = PlayerListeners.getGuiWithPages(contents, name);
	}

	public abstract void onClick(BestiarClickEvent e);

	public abstract String getId();

	public String getName() {
		if (name == null)
			setName(String.valueOf(getId().charAt(0)).toUpperCase() + getId().substring(1));
		return name;
	}

	@EventHandler
	public void onChat(AsyncPlayerChatEvent e) {
		Player hrac = e.getPlayer();
		if (!PLAYERS_CHAT.contains(hrac.getUniqueId())) {
			return;
		}
		e.setCancelled(true);
	}

	public void openInventory() {
		openInventory(0);
	}
	
	public void openInventory(int page) {
		setupInventory();
		if (name == null) {
			setName(String.valueOf(getId().charAt(0)).toUpperCase() + getId().substring(1), true);
		} else {
			update();
		}
		PlayerListeners.openInventory(hrac, inventories);
	}

	public LinkedHashMap<Integer, Inventory> getInventories() {
		return inventories;
	}

	protected final boolean isValid(BestiarClickEvent e) {
		Inventory inv = e.getEvent().getClickedInventory();
		Player hrac = (Player) e.getEvent().getWhoClicked();
		return hrac.getUniqueId().compareTo(e.getPlayer().getUniqueId()) == 0 && ChatColor.stripColor(inv.getName()).equalsIgnoreCase(ChatColor.stripColor(getName()));
	}

	/*
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * */

	public static void registerGui(GUIApi gui, UUID uuid) {
		Bukkit.getPluginManager().registerEvents(gui, Bestiar.getInstance());
		System.out.println("Registering gui " + gui.getId() + " with name " + gui.getName());
		HashMap<String, GUIApi> namem = GUIS_BY_NAME.getOrDefault(uuid, new HashMap<>());
		namem.put(ChatColor.stripColor(gui.getName()), gui);
		GUIS_BY_NAME.put(uuid, namem);
	}

	public static GUIApi getGui(String name, UUID uuid) {
		return GUIS_BY_NAME.get(uuid).get(ChatColor.stripColor(name));
	}

	public static GUIApi getGui(Inventory inv, UUID uuid) {
		return getGui(ChatColor.stripColor(inv.getName()), uuid);
	}

	public static boolean isGui(String name, UUID uuid) {
		return GUIS_BY_NAME.containsKey(uuid) && GUIS_BY_NAME.get(uuid).containsKey(ChatColor.stripColor((String) name));
	}

	public static boolean isGui(Inventory inv, UUID uuid) {
		return isGui(inv.getName(), uuid);
	}
}

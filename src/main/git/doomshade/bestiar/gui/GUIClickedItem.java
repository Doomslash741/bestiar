package main.git.doomshade.bestiar.gui;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

public class GUIClickedItem implements Listener {
	private ItemStack item;
	private String context;
	
	public static final Map<String, GUIClickedItem> ITEMS = new HashMap<>();
	
	public GUIClickedItem(ItemStack item, String context ) {
		this.item = item;
		this.context = context;
		ITEMS.put(item.getItemMeta().getDisplayName(), this);
	}
	
	public ItemStack getItem() {
		return item;
	}
	
	public String getContext() {
		return context;
	}
}

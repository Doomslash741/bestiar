package main.git.doomshade.bestiar.gui.guis;

import java.io.File;
import java.util.LinkedList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import main.git.doomshade.bestiar.Bestiar;
import main.git.doomshade.bestiar.MythicMob;
import main.git.doomshade.bestiar.data.PlayerData;
import main.git.doomshade.bestiar.events.BestiarClickEvent;
import main.git.doomshade.bestiar.gui.GUIApi;
import main.git.doomshade.bestiar.gui.GUIClickedItem;
import main.git.doomshade.bestiar.utils.MythicMobUtils;

public class MobsGUI extends GUIApi {
	private File mobsFile;
	public MobsGUI(String context, Player hrac) {
		super(context, hrac);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return "mobs";
	}

	@Override
	public void setupInventory() {
		// TODO Auto-generated method stub
		LinkedList<GUIClickedItem> inv = new LinkedList<>();
		String[] split = context.toString().split("[" + String.valueOf(SEPARATOR) + "]");
		String fileName = split[0];
		String townName = split[1];
		PlayerData data = Bestiar.getPlayerData(Bukkit.getPlayer(UUID.fromString(split[2])));
		mobsFile = new File(Bestiar.getInstance().getMobsFolder(), fileName + ".yml");
		System.out.println("Initing " + mobsFile.getName());
		FileConfiguration loader = YamlConfiguration.loadConfiguration(mobsFile);
		for (String s : loader.getKeys(false)) {
			MythicMob mm = MythicMobUtils.getInstance().fromConfigName(s);
			if (mm == null) {
				continue;
			}
			if (mm.getLocation().equalsIgnoreCase(townName)) {
				if (data.hasKilled(mm)) {
					inv.add(new GUIClickedItem(mm.getActiveItem(), s));
				} else {
					inv.add(new GUIClickedItem(mm.getInactiveItem(), ""));
				}
			}
		}
		setSetupInventory(inv);
	}

	@Override
	@EventHandler
	public void onClick(BestiarClickEvent e) {
		// TODO Auto-generated method stub
		if (!isValid(e)) {
			return;
		}
		InventoryClickEvent ev = e.getEvent();
		ItemStack item = ev.getCurrentItem();
		if (item == null || !item.hasItemMeta()) {
			return;
		}
		ItemMeta meta = item.getItemMeta();
		if (!meta.hasDisplayName()) {
			return;
		}
		Player hrac = ((Player) ev.getWhoClicked());
		
		MythicMob mm = MythicMobUtils.getInstance().fromDisplayName(meta.getDisplayName());
		if (mm == null) {
			return;
		}
		ItemsGUI gui = new ItemsGUI(mm.getConfigName(), hrac);
		gui.setPreviousGui(this);
		// gui.setName(meta.getDisplayName(), true);
		gui.openInventory();
	}

}

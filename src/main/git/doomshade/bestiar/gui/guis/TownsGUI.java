package main.git.doomshade.bestiar.gui.guis;

import java.util.ArrayList;
import java.util.LinkedList;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import git.doomshade.diablolike.utils.ItemUtils;
import main.git.doomshade.bestiar.Bestiar;
import main.git.doomshade.bestiar.events.BestiarClickEvent;
import main.git.doomshade.bestiar.gui.GUIApi;
import main.git.doomshade.bestiar.gui.GUIClickedItem;

public class TownsGUI extends GUIApi {

	public TownsGUI(String context, Player hrac) {
		super(context, hrac);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void setupInventory() {
		// TODO Auto-generated method stub
		LinkedList<GUIClickedItem> inv = new LinkedList<>();
		for (String mesto : Bestiar.getInstance().getTowns()) {
			inv.add(new GUIClickedItem(ItemUtils.makeItem(Material.APPLE, mesto, new ArrayList<String>(), (short) 0),
					context.toString() + (SEPARATOR + mesto).toString()));
		}
		setSetupInventory(inv);
	}

	@Override
	@EventHandler
	public void onClick(BestiarClickEvent e) {
		// TODO Auto-generated method stub
		if (!isValid(e)) {
			return;
		}

		InventoryClickEvent ev = e.getEvent();
		ItemStack item = ev.getCurrentItem();
		if (item == null || !item.hasItemMeta()) {
			return;
		}
		ItemMeta meta = item.getItemMeta();
		if (!meta.hasDisplayName()) {
			return;
		}
		Player hrac = (Player) ev.getWhoClicked();
		GUIClickedItem guiItem = GUIClickedItem.ITEMS.get(meta.getDisplayName());
		String context = guiItem.getContext() + (SEPARATOR + hrac.getUniqueId().toString()).toString();
		MobsGUI gui = new MobsGUI(context, hrac);
		gui.setPreviousGui(this);
		// gui.setName(meta.getDisplayName(), true);
		gui.openInventory();
	}

	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return "towns";
	}

}

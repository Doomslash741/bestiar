package main.git.doomshade.bestiar.gui.guis;

import java.util.LinkedList;
import java.util.Map.Entry;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import main.git.doomshade.bestiar.Bestiar;
import main.git.doomshade.bestiar.MythicMob;
import main.git.doomshade.bestiar.events.BestiarClickEvent;
import main.git.doomshade.bestiar.gui.GUIApi;
import main.git.doomshade.bestiar.gui.GUIClickedItem;
import main.git.doomshade.bestiar.listeners.PlayerListeners;

public class ItemsGUI extends GUIApi {

	public ItemsGUI(String context, Player hrac) {
		super(context, hrac);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void setupInventory() {
		// TODO Auto-generated method stub
		LinkedList<GUIClickedItem> inv = new LinkedList<>();
		MythicMob mm = Bestiar.getMythicMobUtils().fromConfigName(context.toString());
		for (Entry<Integer, Inventory> a : mm.getDropsInventory().entrySet()) {
			for (ItemStack item : a.getValue()) {
				if (item != null && !item.isSimilar(PlayerListeners.ZPET_BUTTON))
					inv.add(new GUIClickedItem(item, ""));
			}
		}
		setSetupInventory(inv);
	}

	@Override
	@EventHandler
	public void onClick(BestiarClickEvent e) {
		// TODO Auto-generated method stub
		if (!isValid(e)) {
			return;
		}
	}

	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return "items";
	}

}

package main.git.doomshade.bestiar.gui.guis;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import git.doomshade.diablolike.utils.ItemUtils;
import main.git.doomshade.bestiar.events.BestiarClickEvent;
import main.git.doomshade.bestiar.gui.GUIApi;
import main.git.doomshade.bestiar.gui.GUIClickedItem;
import main.git.doomshade.bestiar.utils.MythicMobUtils;

public class MainGUI extends GUIApi {

	public MainGUI(String context, Player hrac) {
		super(context, hrac);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return "main";
	}

	@Override
	public void setupInventory() {
		// TODO Auto-generated method stub
		LinkedList<GUIClickedItem> inv = new LinkedList<>();
		for (File file : MythicMobUtils.getInstance().getFiles()) {
			String name = file.getName().substring(0, file.getName().length() - 4);
			inv.add(new GUIClickedItem(ItemUtils.makeItem(Material.CHEST, name,
					new ArrayList<String>(), (short) 0), name));
		}
		setSetupInventory(inv);
	}

	@Override
	@EventHandler
	public void onClick(BestiarClickEvent e) {
		// TODO Auto-generated method stub
		if (!isValid(e)) {
			return;
		}
		InventoryClickEvent ev = e.getEvent();
		ItemStack item = ev.getCurrentItem();
		if (item == null || !item.hasItemMeta()) {
			return;
		}
		ItemMeta meta = item.getItemMeta();
		if (!meta.hasDisplayName()) {
			return;
		}
		Player hrac = ((Player) ev.getWhoClicked());
		String context = ChatColor.stripColor(meta.getDisplayName());
		TownsGUI gui = new TownsGUI(context, hrac);
		gui.setupInventory();
		gui.setPreviousGui(this);
		//gui.setName(meta.getDisplayName(), true);
		gui.openInventory(); 
	}
}

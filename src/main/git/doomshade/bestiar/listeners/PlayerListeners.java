package main.git.doomshade.bestiar.listeners;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import git.doomshade.diablolike.utils.ItemUtils;
import main.git.doomshade.bestiar.Bestiar;
import main.git.doomshade.bestiar.MythicMob;
import main.git.doomshade.bestiar.events.BestiarClickEvent;
import main.git.doomshade.bestiar.gui.GUIApi;

public class PlayerListeners implements Listener {

	public static final ItemStack ZPET_BUTTON = ItemUtils.makeItem(Material.BARRIER, ChatColor.RED + "Zpet",
			new ArrayList<String>(), (short) 0);
	public static final ItemStack PREVIOUS_PAGE = ItemUtils.makeItem(Material.BOOK, "Predesla stranka",
			new ArrayList<String>(), (short) 0);
	public static final ItemStack NEXT_PAGE = ItemUtils.makeItem(Material.BOOK, "Dalsi stranka",
			new ArrayList<String>(), (short) 0);
	// private static final HashMap<UUID, Integer> CURRENT_PAGE = new HashMap<>();
	private static final Map<UUID, LinkedHashMap<Integer, Inventory>> inventories = new LinkedHashMap<>();
	public static final Map<UUID, Integer> CURRENT_PAGE = new HashMap<>();

	@EventHandler
	public void onKill(EntityDeathEvent e) {
		Player hrac = e.getEntity().getKiller();
		if (hrac == null) {
			return;
		}
		String mobName = e.getEntity().getCustomName();
		MythicMob mm = Bestiar.getMythicMobUtils().fromDisplayName(mobName);
		if (mm == null) {
			return;
		}
		Bestiar.getPlayerData(hrac).addKill(mm);
	}

	@EventHandler
	public void onGUIClick(InventoryClickEvent e) {
		Inventory inv = e.getClickedInventory();
		ItemStack item = e.getCurrentItem();
		Player hrac = (Player) e.getWhoClicked();
		if (inv == null || inv.getName() == null || inv.getName().isEmpty() || !GUIApi.isGui(inv, hrac.getUniqueId()) || item == null) {
			return;
		}

		if (!CURRENT_PAGE.containsKey(hrac.getUniqueId())) {
			return;
		}
		GUIApi gui = GUIApi.getGui(inv, hrac.getUniqueId());
		System.out.println("GUI context: " + gui.getContext().toString());

		e.setCancelled(true);
		int updated = CURRENT_PAGE.get(hrac.getUniqueId());
		int max = gui.getInventories().size() - 1;
		if (item.isSimilar(ZPET_BUTTON)) {
			if (gui.getPreviousGui() != null) {
				System.out.println("Prev gui context: " + gui.getPreviousGui().getContext().toString());
				gui.getPreviousGui().openInventory();
			}
		} else if (item.isSimilar(NEXT_PAGE)) {
			if (updated == max) {
				updated = -1;
			}
			CURRENT_PAGE.put(hrac.getUniqueId(), ++updated);
			hrac.openInventory(gui.getInventories().get(updated));
		} else if (item.isSimilar(PREVIOUS_PAGE)) {
			if (updated == 0) {
				updated = max + 1;
			}
			CURRENT_PAGE.put(hrac.getUniqueId(), --updated);
			hrac.openInventory(gui.getInventories().get(updated));
		} else {
			Bukkit.getPluginManager().callEvent(new BestiarClickEvent(e, hrac));
		}
	}

	public static void openInventory(Player hrac, List<ItemStack> invContents, String invName) {
		openInventory(hrac, invContents, invName, true);
	}

	public static void openInventory(Player hrac, List<ItemStack> invContents, String invName, boolean addZpetButton) {
		openInventory(hrac, getGuiWithPages(invContents, invName, addZpetButton));
	}

	public static void openInventory(Player hrac, LinkedHashMap<Integer, Inventory> map) {
		inventories.put(hrac.getUniqueId(), map);
		CURRENT_PAGE.put(hrac.getUniqueId(), 0);
		hrac.openInventory(map.get(0));
	}

	public static LinkedHashMap<Integer, Inventory> getGuiWithPages(List<ItemStack> invContents, String invName) {
		return getGuiWithPages(invContents, invName, true);
	}

	public static LinkedHashMap<Integer, Inventory> getGuiWithPages(List<ItemStack> invContents, String invName,
			boolean addZpetButton) {
		ArrayList<ItemStack> overLapping;
		int size = 9 - invContents.size() % 9 + invContents.size();
		int invAmount = 1;
		while (size > 51) {
			size -= 54;
			invAmount++;
		}
		LinkedHashMap<Integer, Inventory> inventoriez = new LinkedHashMap<>();
		for (int i = 0; i < invAmount; i++) {
			overLapping = new ArrayList<>();
			Inventory inventory = Bukkit.createInventory(null,
					i == invAmount - 1 ? invContents.size() + 3 + (9 - (invContents.size() + 3) % 9) : 54, invName);
			if (invAmount != 1) {
				int prevPageIndex = inventory.getSize() - 3;
				int nextPageIndex = inventory.getSize() - 2;
				inventory.setItem(prevPageIndex, PREVIOUS_PAGE);
				inventory.setItem(nextPageIndex, NEXT_PAGE);
			}
			if (addZpetButton) {
				int zpetButtonIndex = inventory.getSize() - 1;
				inventory.setItem(zpetButtonIndex, ZPET_BUTTON);
			}
			for (ItemStack itemStack : invContents) {
				if (inventory.firstEmpty() != -1) {
					inventory.addItem(new ItemStack[] { itemStack });
					continue;
				}
				overLapping.add(itemStack);
			}
			invContents = new ArrayList<ItemStack>(overLapping);
			inventoriez.put(i, inventory);
		}
		return inventoriez;
	}
}

package main.git.doomshade.bestiar.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Nullable;

import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.EntityType;

import git.doomshade.diablolike.drops.groups.Dungeon;
import git.doomshade.diablolike.drops.groups.Kolekce;
import git.doomshade.diablolike.utils.DiabloItem;
import git.doomshade.diablolike.utils.Utils;
import main.git.doomshade.bestiar.Bestiar;
import main.git.doomshade.bestiar.MythicMob;

public class MythicMobUtils {
	private static HashMap<String, MythicMob> MYTHIC_MOBS_DISPLAYNAME;
	private static HashMap<String, MythicMob> MYTHIC_MOBS_CONFIGNAME;
	private static ArrayList<MythicMob> MYTHIC_MOBS;
	private static MythicMobUtils instance = null;
	public static final String NEURCENO = "Neurceno";
	private static final Pattern SAPI_EXP_PATTERN = Pattern.compile("skillapi-exp [0-9]+");

	static {
		if (instance == null)
			instance = new MythicMobUtils(Bestiar.getInstance());
	}

	private Bestiar plugin;
	private MythicMobUtils(Bestiar plugin) {
		this.plugin = plugin;
	}

	public void initData() {
		MYTHIC_MOBS_DISPLAYNAME = new HashMap<>();
		MYTHIC_MOBS_CONFIGNAME = new HashMap<>();
		MYTHIC_MOBS = new ArrayList<>();
		FileConfiguration mobsLoader = YamlConfiguration.loadConfiguration(plugin.getMobsFile());
		// TODO Auto-generated method stub
		for (File file : Bestiar.getInstance().getMobsFolder().listFiles()) {
			FileConfiguration loader = YamlConfiguration.loadConfiguration(file);
			for (String key : loader.getKeys(false)) {
				ConfigurationSection mob = loader.getConfigurationSection(key);
				MythicMob mm = fromSection(mob);
				if (mm == null) {
					continue;
				}
				mobsLoader.addDefault(mm.getConfigName(), NEURCENO);
				mm.setLocation(mobsLoader.getString(mm.getConfigName()));
				MYTHIC_MOBS_DISPLAYNAME.put(mm.getName(), mm);
				MYTHIC_MOBS_CONFIGNAME.put(mm.getConfigName(), mm);
				MYTHIC_MOBS.add(mm);
				
			}
		}
		mobsLoader.options().copyDefaults(true);
		try {
			mobsLoader.save(plugin.getMobsFile());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		MYTHIC_MOBS.sort((x, y) -> x.compareTo(y));
	}

	public static MythicMobUtils getInstance() {
		return instance;
	}

	public MythicMob fromDisplayName(String displayName) {
		return MYTHIC_MOBS_DISPLAYNAME.get(displayName);
	}
	
	public MythicMob fromConfigName(String configName) {
		return MYTHIC_MOBS_CONFIGNAME.get(configName);
	}

	public ArrayList<MythicMob> getMythicMobs() {
		return MYTHIC_MOBS;
	}
	
	public File[] getFiles() {
		return plugin.getMobsFolder().listFiles();
	}

	@Nullable
	private MythicMob fromSection(ConfigurationSection mob) {
		String mobName = ChatColor.translateAlternateColorCodes('&', mob.getString("Display"));
		if (mobName.contains("Vyvolany")) {
			return null;
		}
		EntityType type = EntityType.ZOMBIE;
		for (EntityType t : EntityType.values()) {
			if (t.name().equalsIgnoreCase(mob.getString("Type"))) {
				type = t;
				break;
			}
		}
		int health = mob.getInt("Health");
		int damage = mob.getInt("Damage");
		/*
		 * double movementSpeed =
		 * mob.getConfigurationSection("Options").getDouble("MovementSpeed");
		 */
		int exp = 0;
		for (String s : mob.getStringList("Drops")) {
			Matcher m = SAPI_EXP_PATTERN.matcher(s);
			if (m.find()) {
				try {
					exp = Integer.parseInt(m.group().replaceAll("[^0-9]+", ""));
				} catch (NumberFormatException e) {
					System.out.println("Tried to parse " + m.group().replaceAll("[^0-9]+", "")
							+ ". Contact that motherfucker plugin maker for making a mistake.");
				}
			}
		}
		Map<String, List<BestiarItem>> drop = new HashMap<>();

		List<Kolekce> colls = new ArrayList<>();
		String mobN = Utils.DUNGEON_MOB_NAMES.get(ChatColor.stripColor(mobName));
		for (Dungeon dung : Utils.DUNGEONS.values()) {
			if (dung.getDrops().containsKey(mobN)) {
				colls.addAll(dung.getDrops().get(mobN));
			}
		}
		if (Utils.COLLECTIONS.containsKey(Utils.MOB_NAMES.get(ChatColor.stripColor(mobName)))) {
			colls.add(Utils.COLLECTIONS.get(Utils.MOB_NAMES.get(ChatColor.stripColor(mobName))));
		}
		for (Kolekce kolekce : colls) {
			if (kolekce.exists() && kolekce.getKolekce() != null) {
				List<BestiarItem> itemy = new ArrayList<>();
				for (DiabloItem item : kolekce.getKolekce()) {
					if (kolekce.getAmount().getMin() == 0 && kolekce.getAmount().getMax() == 0
							|| kolekce.getChance() == 0d || item.getDropChance() == 0d) {
						continue;
					}
					itemy.add(new BestiarItem(item.getItem(), kolekce.getAmount(), kolekce.getChance()));
				}
				drop.put(kolekce.getName(), itemy);

			}
		}
		System.out.println(mob.getName());
		return new MythicMob(mob.getName(), type, mobName, health, damage, /* movementSpeed, */ exp, drop);
	}

}

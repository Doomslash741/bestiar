package main.git.doomshade.bestiar.utils;

import org.bukkit.inventory.ItemStack;

import git.doomshade.diablolike.drops.Range;

public class BestiarItem {
	public ItemStack item;
	public Range amount;
	public double dropChance;

	public BestiarItem(ItemStack item, Range amount, double dropChance) {
		this.item = item;
		this.amount = amount;
		this.dropChance = dropChance;
	}
}

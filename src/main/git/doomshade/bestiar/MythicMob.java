package main.git.doomshade.bestiar;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import git.doomshade.diablolike.config.TierColor;
import main.git.doomshade.bestiar.listeners.PlayerListeners;
import main.git.doomshade.bestiar.utils.BestiarItem;

public class MythicMob implements Comparable<MythicMob> {
	private EntityType mobType;
	private String displayName, configName;
	private int health, damage, exp;
	private double movementSpeed;
	private Map<String, List<BestiarItem>> drop;
	private List<String> description;
	private List<String> previousItems;
	private ItemStack activeItem, inactiveItem;
	private LinkedHashMap<Integer, Inventory> inv;
	private String location;

	public MythicMob(String configName, EntityType mobType, String displayName, int health, int damage, int exp,
			Map<String, List<BestiarItem>> drop) {
		this(configName, mobType, displayName, health, damage, 0d, exp, drop);
	}

	/**
	 * @param mobType
	 * @param displayName
	 * @param health
	 * @param damage
	 * @param movementSpeed
	 * @param exp
	 * @param drop
	 */
	public MythicMob(String configName, EntityType mobType, String displayName, int health, int damage, double movementSpeed, int exp,
			Map<String, List<BestiarItem>> drop) {
		this.mobType = mobType;
		this.configName = configName;
		this.displayName = displayName;
		this.health = health;
		this.movementSpeed = movementSpeed;
		this.damage = damage;
		this.exp = exp;
		this.drop = drop;
		createDescription();
		createItems();
		createInventory();
	}

	private void createDescription() {
		// TODO Auto-generated method stub
		this.previousItems = new ArrayList<>();
		this.description = new ArrayList<>();
		addStats();
		addDrops();
	}
	
	private void createInventory() {
		List<ItemStack> invContents = new ArrayList<>();
		for (Entry<String, List<BestiarItem>> s : getDrops().entrySet()) {
			for (BestiarItem is : s.getValue()) {
				invContents.add(is.item);
			}
		}
		inv = PlayerListeners.getGuiWithPages(invContents, displayName);
	}

	private void addStats() {
		this.description.add(ChatColor.RED + "Zivoty: " + ChatColor.GOLD + health);
		this.description.add(ChatColor.DARK_RED + "Poskozeni: " + ChatColor.GOLD + damage);
		this.description.add(ChatColor.DARK_GREEN + "Typ: " + ChatColor.GOLD + mobType);
		this.description.add(ChatColor.GREEN + "Exp: " + ChatColor.GOLD + exp);
		if (movementSpeed != 0d) {
			this.description.add(ChatColor.DARK_AQUA + "Movement speed: " + ChatColor.GOLD + movementSpeed);
		}
		this.description.add(ChatColor.DARK_GREEN + "Lokace: &6");
	}

	private void addDrops() {
		this.description.add(ChatColor.BLUE + "Drop:");
		for (String s : drop.keySet()) {
			if (drop.get(s).isEmpty()) {
				continue;
			}
			BestiarItem itemm = drop.get(s).get(0);

			// SinisterKolekce (1-3x, 50.0%)
			this.description
					.add(ChatColor.DARK_AQUA + s + ChatColor.DARK_GRAY + " (" + ChatColor.GRAY + itemm.amount.toString()
							+ "x, " + ChatColor.YELLOW + itemm.dropChance + "%" + ChatColor.DARK_GRAY + ")");
			for (BestiarItem item : drop.get(s)) {
				if (!isValid(item)) {
					continue;
				}
				String itemName = item.item.getItemMeta().getDisplayName();
				this.description.add(ChatColor.GRAY + "- "
						+ (item.item = TierColor.LEGENDARY.setRarity(item.item)).getItemMeta().getDisplayName());
				this.previousItems.add(ChatColor.stripColor(itemName));
			}
		}
	}

	private boolean isValid(BestiarItem item) {
		if (item == null || item.item == null || !item.item.hasItemMeta()
				|| !item.item.getItemMeta().hasDisplayName()) {
			return false;

		}
		String itemName = item.item.getItemMeta().getDisplayName();
		if (previousItems.contains(ChatColor.stripColor(itemName)) || itemName.contains("(Tank)")
				|| itemName.contains("(Heal)")) {
			return false;
		}
		return true;
	}

	private void createItems() {
		Material mat = Material.SKULL_ITEM;
		short data = 3;

		switch (mobType) {
		case SKELETON:
			data = 0;
			break;
		case WITHER_SKULL:
			data = 1;
			break;
		case ZOMBIE:
			data = 2;
			break;
		case CREEPER:
			data = 4;
			break;
		case ENDER_DRAGON:
			data = 5;
			break;
		default:
			break;
		}

		activeItem = new ItemStack(mat, 1, data);
		SkullMeta meta = (SkullMeta) activeItem.getItemMeta();
		if (mobType == EntityType.PIG_ZOMBIE)
			meta.setOwner("Pigman");
		meta.setDisplayName(displayName);
		meta.setLore(description);
		activeItem.setItemMeta(meta);
		inactiveItem = activeItem.clone();
		ItemMeta m = inactiveItem.getItemMeta();
		meta.setLore(Arrays.asList(ChatColor.DARK_AQUA + "?"));
		inactiveItem.setItemMeta(m);
	}

	public ItemStack getActiveItem() {
		return activeItem;
	}

	public ItemStack getInactiveItem() {
		return inactiveItem;
	}

	public Map<String, List<BestiarItem>> getDrops() {
		return drop;
	}
	
	public String getConfigName() {
		return configName;
	}

	@Override
	public int compareTo(MythicMob o) {
		// TODO Auto-generated method stub
		if (o.health > health) {
			return -1;
		} else if (o.health < health) {
			return 1;
		}
		return 0;
	}

	public String getName() {
		// TODO Auto-generated method stub
		return displayName;
	}
	
	public LinkedHashMap<Integer, Inventory> getDropsInventory() {
		return inv;
	} 
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	public String getLocation() {
		return location;
	}
}

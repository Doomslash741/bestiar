package test.git.doomshade.bestiar.gui.guis;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;

import test.git.doomshade.bestiar.MythicMob;
import test.git.doomshade.bestiar.data.PlayerData;
import test.git.doomshade.bestiar.events.BestiarClickEvent;
import test.git.doomshade.bestiar.gui.GUI;
import test.git.doomshade.bestiar.utils.MythicMobUtils;

public class MobsGui extends GUI {

	public MobsGui(Player hrac, Context context) {
		super(hrac, context);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Mobs Gui";
	}

	@Override
	protected List<ItemStack> getContents() {
		// TODO Auto-generated method stub
		List<ItemStack> items = new ArrayList<>();
		MythicMobUtils mmu = MythicMobUtils.getInstance();
		String mobFileName = getContextsContext().getLast();
		File mobFile = mmu.getMobsFileFromInventoryName(mobFileName);
		if (!mobFile.exists()) {
			return items;
		}
		
		List<MythicMob> mobs = new ArrayList<>();
		PlayerData data = PlayerData.getPlayerData(getPlayer());
		FileConfiguration loader = YamlConfiguration.loadConfiguration(mobFile);
		for (String s : loader.getKeys(false)) {
			MythicMob mm = mmu.fromConfigName(s);
			if (mm == null) {
				continue;
			}
			mobs.add(mm);
		}
		
		mobs.sort((x, y) -> x.compareTo(y));
		
		for (MythicMob mm : mobs) {
			if (data.hasKilled(mm)) {
				items.add(mm.getActiveItem());
			} else {
				items.add(mm.getInactiveItem());
			}
		}
		return items;
	}

	@Override
	public Class<? extends GUI> getNextGui() {
		// TODO Auto-generated method stub
		return ItemsGui.class;
	}

	@Override
	@EventHandler
	public void onClick(BestiarClickEvent e) {
		// TODO Auto-generated method stub
		if (!isValidEvent(e) || !isValidItem(e, true, false)) {
			return;
		}
		String name = e.getEvent().getCurrentItem().getItemMeta().getDisplayName();
		MythicMobUtils mmu = MythicMobUtils.getInstance();
		MythicMob mob = mmu.fromDisplayName(name);
		if (mob == null) {
			return;
		}
		if (!PlayerData.getPlayerData(getPlayer()).hasKilled(mob)) {
			return;
		}
		openNextGuiAndAddContext(e, name);
		
	}

}

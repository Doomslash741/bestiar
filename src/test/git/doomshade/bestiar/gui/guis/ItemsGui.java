package test.git.doomshade.bestiar.gui.guis;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;

import test.git.doomshade.bestiar.MythicMob;
import test.git.doomshade.bestiar.events.BestiarClickEvent;
import test.git.doomshade.bestiar.gui.GUI;
import test.git.doomshade.bestiar.utils.BestiarItem;
import test.git.doomshade.bestiar.utils.MythicMobUtils;

public class ItemsGui extends GUI {

	public ItemsGui(Player hrac, Context context) {
		super(hrac, context);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Class<? extends GUI> getNextGui() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected List<ItemStack> getContents() {
		// TODO Auto-generated method stub
		List<ItemStack> items = new ArrayList<>();
		String mobName = getContextsContext().getLast();
		MythicMob mm = MythicMobUtils.getInstance().fromDisplayName(mobName);
		if (mm == null) {
			getPlayer().sendMessage("Nastala nejaka chyba, kontaktuj prosim admina Doomshade a vyrid mu \"ItemsGui;mm == null\"");
			return items;
		}
		Map<String, List<BestiarItem>> map = mm.getDrops();
		for (String s : map.keySet()) {
			for (BestiarItem item : map.get(s)) {
				items.add(item.item);
			}
		}
		return items;
	}

	@Override
	@EventHandler
	public void onClick(BestiarClickEvent e) {
		// TODO Auto-generated method stub
		
		if (!isValidEvent(e) || !isValidItem(e, true, false)) {
			return;
		}
		//String name = ChatColor.stripColor(e.getEvent().getCurrentItem().getItemMeta().getDisplayName());
		//openNextGuiAndAddContext(e, name);
	}
	

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Drop";
	}

}

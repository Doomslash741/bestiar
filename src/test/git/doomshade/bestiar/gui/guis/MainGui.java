package test.git.doomshade.bestiar.gui.guis;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;

import git.doomshade.diablolike.utils.ItemUtils;
import test.git.doomshade.bestiar.events.BestiarClickEvent;
import test.git.doomshade.bestiar.gui.GUI;
import test.git.doomshade.bestiar.utils.MythicMobUtils;

public class MainGui extends GUI {

	public MainGui(Player hrac, Context context) {
		super(hrac, context);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected List<ItemStack> getContents() {
		// TODO Auto-generated method stub
		List<ItemStack> items = new ArrayList<>();
		MythicMobUtils mmu = MythicMobUtils.getInstance();
		for (File file : mmu.getFiles()) {
			items.add(
					ItemUtils.makeItem(Material.CHEST, mmu.getMobsFileName(file), new ArrayList<String>(), (short) 0));
		}
		return items;
	}

	@Override
	public Class<? extends GUI> getNextGui() {
		// TODO Auto-generated method stub
		return MobsGui.class;
	}

	@Override
	@EventHandler
	public void onClick(BestiarClickEvent e) {
		// TODO Auto-generated method stub
		boolean isValidEvent = isValidEvent(e);
		boolean isValidItem = isValidItem(e, true, false);
		if (!isValidEvent || !isValidItem) {
			return;
		}
		String name = e.getEvent().getCurrentItem().getItemMeta().getDisplayName();
		
		openNextGuiAndAddContext(e, name);
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Main Gui";
	}
}

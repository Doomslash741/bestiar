package test.git.doomshade.bestiar.gui;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;

import javax.annotation.Nullable;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import test.git.doomshade.bestiar.Bestiar;
import test.git.doomshade.bestiar.events.BestiarClickEvent;
import test.git.doomshade.bestiar.listeners.PlayerListeners;

public abstract class GUI implements Listener {

	private final Player hrac;
	private final UUID uuid;
	private static final LinkedHashMap<UUID, LinkedList<GUI>> OPENED_GUIS = new LinkedHashMap<>();
	private static final LinkedHashMap<UUID, LinkedList<GUI>> REGISTERED_GUIS = new LinkedHashMap<>();
	private Context context;
	private LinkedHashMap<Integer, Inventory> inv;
	private int page;
	private String name;
	private final boolean isMain = getClass().getSimpleName().toLowerCase().startsWith("main");

	public GUI(Player hrac, Context context) {
		this.hrac = hrac;
		this.uuid = hrac.getUniqueId();
		this.context = context;
		this.name = getName();
		update();
		LinkedList<GUI> registeredGuis = REGISTERED_GUIS.getOrDefault(uuid, new LinkedList<>());
		registeredGuis.add(this);
		REGISTERED_GUIS.put(uuid, registeredGuis);
		Bukkit.getPluginManager().registerEvents(this, Bestiar.getInstance());
	}

	public static void unregister(Player hrac) {
		UUID uuid = hrac.getUniqueId();
		if (OPENED_GUIS.containsKey(uuid)) {
			for (GUI gui : OPENED_GUIS.get(uuid)) {
				HandlerList.unregisterAll(gui);
			}
			OPENED_GUIS.remove(uuid);
		}
		if (REGISTERED_GUIS.containsKey(uuid)) {
			for (GUI gui : REGISTERED_GUIS.get(uuid)) {
				HandlerList.unregisterAll(gui);
			}
			REGISTERED_GUIS.remove(uuid);
		}
		
	}

	public void update() {
		this.inv = PlayerListeners.getGuiWithPages(getContents(), name, !isMain());
		this.page = 0;
	}

	private boolean isMain() {
		return isMain;
	}

	public abstract String getName();

	protected <T extends GUI> GUI getGui(Class<T> gui) {
		return getGui(hrac, gui, context);
	}

	private Inventory getOpenedInventory() {
		if (inv != null && inv.containsKey(page)) {
			return inv.get(page);
		}
		throw new IllegalStateException(name + " GUI has no page " + page + " !");
	}

	public final Context getContext() {
		return context;
	}

	public final LinkedList<String> getContextsContext() {
		return context.getContext();
	}

	protected final boolean isValidEvent(BestiarClickEvent e) {
		return e.getPlayer().getUniqueId().compareTo(uuid) == 0
				&& e.getEvent().getInventory().getName().equals(getName());
	}

	protected final boolean isValidItem(BestiarClickEvent e, boolean displayName, boolean lore) {
		InventoryClickEvent ev = e.getEvent();
		ItemStack item = ev.getCurrentItem();
		if (item == null || !item.hasItemMeta()) {
			return false;
		}
		ItemMeta meta = item.getItemMeta();
		return !((!meta.hasDisplayName() && displayName) || (!meta.hasLore() && lore));
	}

	protected final boolean openNextGuiAndAddContext(BestiarClickEvent e, String context) {
		InventoryClickEvent ev = e.getEvent();
		ItemStack item = ev.getCurrentItem();
		if (item == null || !item.hasItemMeta()) {
			return false;
		}
		ItemMeta meta = item.getItemMeta();
		if (!meta.hasDisplayName()) {
			return false;
		}
		getContext().addContext(context);
		return openNextGui();
	}

	public final boolean openGui() {
		LinkedList<GUI> openedGuis = isMain() ? new LinkedList<>() : OPENED_GUIS.getOrDefault(uuid, new LinkedList<>());
		
		if (!openedGuis.contains(this)) {
			openedGuis.offerLast(this);
			OPENED_GUIS.put(uuid, openedGuis);
		}
		hrac.openInventory(getOpenedInventory());
		return true;
	}

	public final boolean openGui(int page) {
		this.page = page;
		return inv != null && inv.containsKey(page) && hrac.openInventory(getOpenedInventory()) != null;
	}

	public final boolean openNextInventory() {
		return openGui((page == inv.size() - 1) ? (page = 0) : ++page);
	}

	public final boolean openPreviousInventory() {
		return openGui(page == 0 ? (page = inv.size() - 1) : --page);
	}

	public final boolean openNextGui() {
		GUI gui = getGui(getNextGui());
		return gui != null && gui.openGui();
	}

	public final boolean openPreviousGui() {
		if (!OPENED_GUIS.containsKey(uuid)) {
			return false;
		}
		LinkedList<GUI> guis = OPENED_GUIS.get(uuid);
		guis.pollLast();
		GUI lastGui = guis.peekLast();
		lastGui.context.VALUES.pollLast();
		OPENED_GUIS.put(uuid, guis);
		return lastGui != null && lastGui.openGui();
	}

	public final void setName(String name) {
		this.name = name;
	}

	public final int getPage() {
		return page;
	}

	public final Player getPlayer() {
		return hrac;
	}

	public static GUI getGui(Player hrac, Class<? extends GUI> gui) {
		return getGui(hrac, gui, new Context());
	}

	public static GUI getOpenedGui(Player hrac) {
		return OPENED_GUIS.getOrDefault(hrac.getUniqueId(), new LinkedList<>()).peekLast();
	}

	public static GUI getGui(Player hrac, Class<? extends GUI> gui, Context context) {
		if (gui == null) {
			return null;
		}
		if (!REGISTERED_GUIS.containsKey(hrac.getUniqueId())) {
			return instantiateGui(hrac, gui, context);
		}

		for (GUI ogui : REGISTERED_GUIS.get(hrac.getUniqueId())) {
			if (ogui.getClass().getName().equals(gui.getName())) {
				ogui.update();
				return ogui;
			}
		}

		return instantiateGui(hrac, gui, context);
	}

	private static GUI instantiateGui(Player hrac, Class<? extends GUI> gui, Context context) {
		try {
			Constructor<? extends GUI> c = gui.getConstructor(Player.class, Context.class);
			c.setAccessible(true);
			return c.newInstance(hrac, context);
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException e) {
			Bukkit.getLogger().log(Level.SEVERE, "Could not find a GUI!", e);
			// TODO Auto-generated catch block
			return null;
		}
	}

	protected abstract List<ItemStack> getContents();

	@Nullable
	public abstract Class<? extends GUI> getNextGui();

	public abstract void onClick(BestiarClickEvent e);

	protected static class Context {
		private final LinkedList<String> VALUES = new LinkedList<>();

		public Context addContext(String context) {
			VALUES.add(context);
			return this;
		}

		public LinkedList<String> getContext() {
			return VALUES;
		}
	}

}

package test.git.doomshade.bestiar.listeners;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import git.doomshade.diablolike.utils.ItemUtils;
import git.doomshade.parties.Parties;
import git.doomshade.parties.party.Parta;
import test.git.doomshade.bestiar.Bestiar;
import test.git.doomshade.bestiar.MythicMob;
import test.git.doomshade.bestiar.events.BestiarClickEvent;
import test.git.doomshade.bestiar.gui.GUI;

public class PlayerListeners implements Listener {

	public static final ItemStack ZPET_BUTTON = ItemUtils.makeItem(Material.BARRIER, ChatColor.RED + "Zpet",
			new ArrayList<String>(), (short) 0);
	public static final ItemStack PREVIOUS_PAGE = ItemUtils.makeItem(Material.BOOK, "Predesla stranka",
			new ArrayList<String>(), (short) 0);
	public static final ItemStack NEXT_PAGE = ItemUtils.makeItem(Material.BOOK, "Dalsi stranka",
			new ArrayList<String>(), (short) 0);

	@EventHandler
	public void onLeave(PlayerQuitEvent e) {
		Player hrac = e.getPlayer();
		GUI.unregister(hrac);
	}

	@EventHandler
	public void onKill(EntityDeathEvent e) {
		Player hrac = e.getEntity().getKiller();
		if (hrac == null) {
			return;
		}
		String mobName = e.getEntity().getCustomName();
		MythicMob mm = Bestiar.getMythicMobUtils().fromDisplayName(mobName);
		if (mm == null) {
			return;
		}
		Parta parta = Parties.getInstance().getParty(hrac);
		if (parta != null) {
			for (UUID uuid : parta) {
				Bestiar.getPlayerData(Bukkit.getPlayer(uuid)).addKill(mm);
			}
		} else
			Bestiar.getPlayerData(hrac).addKill(mm);
	}

	@EventHandler
	public void onGUIClick(InventoryClickEvent e) {
		Inventory inv = e.getWhoClicked().getOpenInventory().getTopInventory();
		ItemStack item = e.getCurrentItem();
		Player hrac = (Player) e.getWhoClicked();
		if (inv == null || inv.getName() == null || inv.getName().isEmpty() || item == null) {
			return;
		}
		GUI gui = GUI.getOpenedGui(hrac);
		if (gui == null || !ChatColor.stripColor(gui.getName()).equalsIgnoreCase(ChatColor.stripColor(inv.getName()))) {
			return;
		}

		e.setCancelled(true);
		if (item.isSimilar(ZPET_BUTTON)) {
			gui.openPreviousGui();
		} else if (item.isSimilar(NEXT_PAGE)) {
			gui.openNextInventory();
		} else if (item.isSimilar(PREVIOUS_PAGE)) {
			gui.openPreviousInventory();
		} else {
			Bukkit.getPluginManager().callEvent(new BestiarClickEvent(e, hrac));
		}
	}

	public static LinkedHashMap<Integer, Inventory> getGuiWithPages(List<ItemStack> invContents, String invName) {
		return getGuiWithPages(invContents, invName, true);
	}

	public static LinkedHashMap<Integer, Inventory> getGuiWithPages(List<ItemStack> invContents, String invName,
			boolean addZpetButton) {
		ArrayList<ItemStack> overLapping;
		int size = 9 - invContents.size() % 9 + invContents.size();
		int invAmount = 1;
		while (size > 51) {
			size -= 54;
			invAmount++;
		}
		LinkedHashMap<Integer, Inventory> inventoriez = new LinkedHashMap<>();
		for (int i = 0; i < invAmount; i++) {
			overLapping = new ArrayList<>();
			Inventory inventory = Bukkit.createInventory(null,
					i == invAmount - 1 ? invContents.size() + 3 + (9 - (invContents.size() + 3) % 9) : 54, invName);
			if (invAmount != 1) {
				int prevPageIndex = inventory.getSize() - 3;
				int nextPageIndex = inventory.getSize() - 2;
				inventory.setItem(prevPageIndex, PREVIOUS_PAGE);
				inventory.setItem(nextPageIndex, NEXT_PAGE);
			}
			if (addZpetButton) {
				int zpetButtonIndex = inventory.getSize() - 1;
				inventory.setItem(zpetButtonIndex, ZPET_BUTTON);
			}
			for (ItemStack itemStack : invContents) {
				if (inventory.firstEmpty() != -1) {
					inventory.addItem(new ItemStack[] { itemStack });
					continue;
				}
				overLapping.add(itemStack);
			}
			invContents = new ArrayList<ItemStack>(overLapping);
			inventoriez.put(i, inventory);
		}
		return inventoriez;
	}
}

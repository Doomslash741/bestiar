package test.git.doomshade.bestiar.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import test.git.doomshade.bestiar.Bestiar;
import test.git.doomshade.bestiar.MythicMob;
import test.git.doomshade.bestiar.gui.GUI;
import test.git.doomshade.bestiar.gui.guis.MainGui;

public class PlayerData {
	private static Bestiar plugin;
	private static FileConfiguration loader;
	public static final HashMap<UUID, PlayerData> PLAYERS = new HashMap<>();
	private static int task = -1;
	private List<String> activeNames;
	private final Player hrac;
	private final UUID uuid;

	static {
		plugin = Bestiar.getInstance();
		loader = YamlConfiguration.loadConfiguration(plugin.getPlayerFile());
	}

	public static void unregister(Player hrac) {
		UUID uuid = hrac.getUniqueId();
		if (PLAYERS.containsKey(uuid)) {
			PLAYERS.remove(uuid);
		}
	}

	private PlayerData(final Player hrac) {
		this.hrac = hrac;
		this.uuid = hrac.getUniqueId();
		if (!loader.isList(uuid.toString())) {
			loader.set(uuid.toString(), new ArrayList<String>());
		}
		this.activeNames = new ArrayList<>(loader.getStringList(uuid.toString()));
		PLAYERS.put(hrac.getUniqueId(), this);
	}

	public static int getTask() {
		return task;
	}

	public static PlayerData getPlayerData(Player hrac) {
		return PLAYERS.getOrDefault(hrac, new PlayerData(hrac));
	}

	public List<String> getKilledMobNames() {
		return activeNames;
	}

	public boolean hasKilled(MythicMob mob) {
		return hasKilled(mob.getName());
	}

	public boolean hasKilled(String mobName) {
		if (mobName.isEmpty()) {
			return false;
		}
		return activeNames.contains(ChatColor.stripColor(mobName)) || isOp();
	}

	public boolean isOp() {
		return hrac.isOp() || hrac.hasPermission("bestiar.*");
	}

	public void addKill(MythicMob mob) {
		
		if (hasKilled(mob)) {
			return;
		}
		if (hrac.isOnline()) {
			hrac.sendMessage(mob.getName() + ChatColor.RESET + " p�id�n do Besti��e");
		}
		String mobName = ChatColor.stripColor(mob.getName());
		activeNames.add(mobName);
		loader.set(uuid.toString(), activeNames);
	}

	public void openInventory() {
		GUI.getGui(hrac.getPlayer(), MainGui.class).openGui();
	}

	public static void loadData() {
		task = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					loader.save(plugin.getPlayerFile());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}, 0, Bestiar.PERIOD);
	}

	public static void save() throws IOException {
		loader.save(plugin.getPlayerFile());
	}
}

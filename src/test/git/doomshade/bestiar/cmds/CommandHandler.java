package test.git.doomshade.bestiar.cmds;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import test.git.doomshade.bestiar.Bestiar;

public class CommandHandler implements CommandExecutor, TabCompleter {
	private static Bestiar plugin = Bestiar.getInstance();
	private static final PluginCommand cmd = plugin.getCommand("bestiar");
	public static Map<String, ICommand> CMDS;
	private static CommandHandler instance;

	static {
		if (instance == null) {
			instance = new CommandHandler();
		}
	}

	/**
	 * Register commandhandleru a commandu
	 */
	public static void setup() {
		CMDS = new TreeMap<>();
		cmd.setExecutor(instance);
		registerCommands();
		CMDS = new TreeMap<>(CMDS);
	}

	private static void registerCommands() {
		// TODO Auto-generated method stub
		// registerCommand(new ReloadCommand());
		registerCommand(new ReloadCommand());
		registerCommand(new MenuCommand());

	}

	public static void registerCommand(ICommand icmd) {
		CMDS.put(icmd.getCommand(), icmd);
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		// TODO Auto-generated method stub
		List<String> tab = new ArrayList<>();
		if (args.length == 0) {
			CMDS.forEach((x, y) -> {
				if (isValid(sender, y)) {
					tab.add(x);
				}
			});
			return tab;
		}
		for (ICommand icmd : CMDS.values()) {
			if (!isValid(sender, icmd)) {
				continue;
			}

			if (icmd.getCommand().startsWith(args[0])) {

				// napr. /horses reload (vrati argumenty toho commandu - v tomto pripade tridy
				// ReloadCommand a to zrovna null, protoze reload
				// command nema zadne argumenty)
				if (icmd.getCommand().equalsIgnoreCase(args[0])) {
					return icmd.onTabComplete(sender, cmd, label, args);
				}

				// /horses re (nabidne /horses REload)
				tab.add(icmd.getCommand());
			}
		}

		return tab.isEmpty() ? null : tab;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		// TODO Auto-generated method stub

		// /horses only
		if (args.length == 0) {

			// je to jen peknej message :D
			sender.sendMessage(ChatColor.DARK_AQUA + "" + ChatColor.STRIKETHROUGH + "-------" + ChatColor.DARK_AQUA
					+ "[" + ChatColor.RED + plugin.getName() + ChatColor.DARK_AQUA + "]" + ChatColor.STRIKETHROUGH
					+ "-------");
			for (ICommand icmd : CMDS.values()) {
				if (!isValid(sender, icmd)) {
					continue;
				}
				sender.sendMessage(infoMessage(icmd));
			}
			return true;
		}

		if (!CMDS.containsKey(args[0])) {
			// Teoreticky sem muzu napsat nejaky error message, ze dotycny napsal spatne cmd
			return false;
		}
		ICommand icmd = CMDS.get(args[0]);

		if (!isValid(sender, icmd)) {
			// Sem taky, ze nemuze pouzit dany cmd
			return false;
		}

		// cmd musi mit vsechny dulezite argumenty, pokud nejake ma
		List<String> cmdArgs = icmd.getArgs() != null && icmd.getArgs().containsKey(true)
				? new ArrayList<>(icmd.getArgs().get(true))
				: null;
		if (cmdArgs != null && cmdArgs.size() > args.length - 1) {
			// Pokud nema, napise to, jak ten cmd ma vypadat
			sender.sendMessage(infoMessage(icmd));
			return true;
		}

		// Vsechno je v poradku at this point (cmd existuje, sender ho muze napsat, ma
		// vsechny argumenty -> execute)
		return icmd.onCommand(sender, cmd, label, args);

	}

	/**
	 * @param icmd
	 * @return /horses command <args-true> [args-false]
	 */
	public static String infoMessage(ICommand icmd) {
		// /horses summon
		StringBuilder builder = new StringBuilder(ChatColor.DARK_AQUA + "/" + cmd.getName() + " " + icmd.getCommand());
		if (icmd.getArgs() != null) {
			if (icmd.getArgs().containsKey(true))

				// /horses summon <horse>
				icmd.getArgs().get(true).forEach(x -> {
					builder.append(" <");
					builder.append(x);
					builder.append(">");
				});
			if (icmd.getArgs().containsKey(false))

				// SummonCommand nema zadne optional argumenty
				icmd.getArgs().get(false).forEach(x -> {
					builder.append(" [");
					builder.append(x);
					builder.append("]");
				});
		}

		// /horses summon <horse> - summons a horse
		return builder.append(ChatColor.GOLD + " - " + icmd.getDescription()).toString();
	}

	// Basically if the sender can send the command
	private static boolean isValid(CommandSender sender, ICommand acmd) {
		return !((acmd.requiresOp() && !sender.isOp()) || (acmd.requiresPlayer() && !(sender instanceof Player)));
	}

}

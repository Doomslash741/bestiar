package test.git.doomshade.bestiar.cmds;

import java.util.List;
import java.util.Map;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public interface ICommand {

	public String getCommand();

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args);

	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args);

	public Map<Boolean, List<String>> getArgs();

	public String getDescription();

	public boolean requiresPlayer();

	public boolean requiresOp();

}

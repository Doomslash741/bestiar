package test.git.doomshade.bestiar;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import test.git.doomshade.bestiar.cmds.CommandHandler;
import test.git.doomshade.bestiar.data.PlayerData;
import test.git.doomshade.bestiar.listeners.PlayerListeners;
import test.git.doomshade.bestiar.utils.MythicMobUtils;

public class Bestiar extends JavaPlugin implements Listener {
	private static Bestiar instance;
	private File playersFile, mobsFolder, mobsFile, menuFile;
	public static final long PERIOD = 20 * 120;

	public File getMobsFolder() {
		return mobsFolder;
	}

	@Override
	public void onEnable() {
		setInstance(this);
		// TODO Auto-generated method stub
		saveDefaultConfig();
		init();
		CommandHandler.setup();
		Bukkit.getPluginManager().registerEvents(new PlayerListeners(), this);
		System.out.println(getTowns());

	}

	public static PlayerData getPlayerData(Player hrac) {
		return PlayerData.getPlayerData(hrac);
	}

	public void init() {
		initFiles();
		getMythicMobUtils().initData();
	}

	public static MythicMobUtils getMythicMobUtils() {
		return MythicMobUtils.getInstance();
	}

	public File getPlayerFile() {
		return playersFile;
	}

	public File getMobsFile() {
		return mobsFile;
	}

	public File getMenuFile() {
		return menuFile;
	}

	public Map<Integer, String> getTowns() {
		Map<Integer, String> towns = new HashMap<>();
		for (String s : getConfig().getStringList("mesta")) {
			towns.put(towns.size(), s);
		}
		return towns;
	}

	private void initFiles() {

		if (!getDataFolder().isDirectory()) {
			getDataFolder().mkdirs();
		}

		this.mobsFolder = new File(getDataFolder(), "Mobs");
		if (!mobsFolder.isDirectory()) {
			mobsFolder.mkdir();
		}
		
		this.playersFile = new File(getDataFolder(), "playerdata.yml");
		if (!playersFile.exists()) {
			try {
				playersFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		mobsFile = new File(getDataFolder(), "mobs.yml");
		if (!mobsFile.exists()) {
			try {
				mobsFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		menuFile = new File(getDataFolder(), "menu.yml");
		if (!menuFile.exists()) {
			try {
				menuFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onDisable() {
		Bukkit.getScheduler().cancelTask(PlayerData.getTask());
		// TODO Auto-generated method stub
		try {
			PlayerData.save();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @return the instance
	 */
	public static Bestiar getInstance() {
		return instance;
	}

	/**
	 * @param instance
	 *            the instance to set
	 */
	private static void setInstance(Bestiar instance) {
		Bestiar.instance = instance;
	}
}
